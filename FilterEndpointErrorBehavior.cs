﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Xml;

namespace FilterEndpointError
{
    public class FilterEndpointErrorBehavior : IEndpointBehavior, IClientMessageInspector
    {
        public string _routeDirectToTP;

        public FilterEndpointErrorBehavior(string RouteDirectToTP)
        {
            System.Diagnostics.EventLog.WriteEntry("FilterEndpointErrorBehavior", "in FilterEndpointErrorBehavior");
             _routeDirectToTP = RouteDirectToTP;
         
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            System.Diagnostics.EventLog.WriteEntry("BeforeSendRequest", "in BeforeSendRequest");
            return null;
        }

        private HttpRequestMessageProperty GetOrCreateHttpProperty(Message request)
        {
            return new HttpRequestMessageProperty();
        }

        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            System.Diagnostics.EventLog.WriteEntry("AfterReceiveReply", "in AfterReceiveReply");

            HttpRequestMessageProperty httpProp = GetOrCreateHttpProperty(reply);

            foreach (var property in reply.Properties)
            {
                if (property.Key == "httpResponse" && property.Value.ToString().Contains("OK"))
                    return;
                else
                {
                     MemoryStream memStream = new MemoryStream();

                    XmlDictionaryWriter xdw = XmlDictionaryWriter.CreateBinaryWriter(memStream);
                    xdw.WriteStartElement("ErrorResponse", "http://tempuri.org/");
                    xdw.WriteStartElement("Result", "http://tempuri.org/");
                    xdw.WriteAttributeString("property", property.Key);
                    xdw.WriteString(property.Value.ToString());
                    xdw.WriteEndElement();
                    xdw.WriteEndElement();
                    xdw.Flush();

                    memStream.Position = 0;

                    XmlDictionaryReaderQuotas quotas = new XmlDictionaryReaderQuotas();
                    XmlDictionaryReader xdr = XmlDictionaryReader.CreateBinaryReader(memStream, quotas);
                    Message replacedMessage = Message.CreateMessage(reply.Version, null, xdr);
                    replacedMessage.Headers.CopyHeadersFrom(reply.Headers);
                    replacedMessage.Properties.CopyProperties(reply.Properties);
                    reply = replacedMessage;
                }


            }

            if (!String.IsNullOrEmpty(_routeDirectToTP))
            {
                httpProp.Headers.Add("RouteDirectToTP", _routeDirectToTP);
            }
        }



        void IEndpointBehavior.AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
         
        }

        void IEndpointBehavior.ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(this);
        }

        void IEndpointBehavior.ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
           
        }

        void IEndpointBehavior.Validate(ServiceEndpoint endpoint)
        {
        
        }
    }
    public class FilterEndpointErrorBehaviorElement : BehaviorExtensionElement
    {
        protected override object CreateBehavior()
        {
            return new FilterEndpointErrorBehavior(RouteDirectToTP);
        }

        [ConfigurationProperty("RouteDirectToTP")]
        public string RouteDirectToTP
        {
            get { return (string)base["RouteDirectToTP"]; }
            set { base["RouteDirectToTP"] = value; }
        }
               
        public override Type BehaviorType
        {
            get { return typeof(FilterEndpointErrorBehavior); }
        }

    }
}
